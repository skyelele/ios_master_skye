//
//  ViewController.swift
//  Quizzler
//
//  Created by Angela Yu on 25/08/2015.
//  Copyright (c) 2015 London App Brewery. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    //The UI Elements from the storyboard are already linked up for you.
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet var progressBar: UIView!
    @IBOutlet weak var progressLabel: UILabel!

    let allQuestions = QuestionBank()
    var pickedAnswer : Bool = false
    var questionNumber : Int = 0
    var score : Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        updateUI()
    }

    @IBAction func answerPressed(_ sender: AnyObject) {

        if sender.tag == 1 {
            pickedAnswer = true
        }

        else if sender.tag == 2 {
            pickedAnswer = false
        }

        checkAnswer()

        questionNumber = questionNumber + 1

        updateUI()
    }

    func updateUI() {

        progressBar.frame.size.width = (view.frame.size.width / 13) * CGFloat(questionNumber)

        progressLabel.text = String(questionNumber) + "13"

        scoreLabel.text = "Score: " + String(score)

        nextQuestion()
    }

    func nextQuestion() {

        if questionNumber <= 12 {
            questionLabel.text = allQuestions.list[questionNumber].questionText
        }
        else {
            let alert = UIAlertController(title; "Awesome", message: "You've finished all the questions, do you want to start over?", preferredStyle; .alert)

            let restartAction = UIAlertAction(title: "Restart", style:.default, handler: { UIALertAction in self.startOver()})
        }
    }

}